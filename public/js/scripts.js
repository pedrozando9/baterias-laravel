var dados =[
    {
        pilha: "Pilha Alcalina Duracel AA",
        tensao_nominal: 1.5,
        corrente: 2800.0,
        E: 1.331,
        V: 1.338,
        R: 23.8,
    },
    {
        pilha: "Bateria Elgin",
        tensao_nominal: 9.0,
        corrente: 250.0,
        E: 7.35,
        V: 2.97,
        R: 23.8,
    },
    {
        pilha: "Bateria Golite",
        tensao_nominal: 9.0,
        corrente: 500.0,
        E: 5.96,
        V: 3.12,
        R: 23.8,
    },
    {
        pilha: "Pilha JYX",
        tensao_nominal: 4.2,
        corrente: 9800.0,
        E: 2.81,
        V: 2.77,
        R: 23.8,
    },
    {
        pilha: "Pilha Luatek",
        tensao_nominal: 3.7,
        corrente: 1200.0,
        E: 2.61,
        V: 2.53,
        R: 23.8,
    },
    {
        pilha: "Pilha Panasonic AA",
        tensao_nominal: 1.5,
        corrente: 2800.0,
        E: 1.41,
        V: 1.27,
        R: 23.8,
    },
    {
        pilha: "Pilha Philips AAA",
        tensao_nominal: 1.5,
        corrente: 1200.0,
        E: 1.38,
        V: 1.338,
        R: 23.8,
    },
    {
        pilha: "Pilha Alcalina Duracel AAA",
        tensao_nominal: 1.5,
        corrente: 1200.0,
        E: 0.993,
        V: 0.809,
        R: 23.8,
    },
    {
        pilha: "Unipower",
        tensao_nominal: 12.0,
        corrente: 7.0,
        E: 10.50,
        V: 10.35,
        R: 23.8,
    },
    {
        pilha: "FreeDown",
        tensao_nominal: 12.0,
        corrente: 30.0,
        E: 10.69,
        V: 10.49,
        R: 23.8,
    },
];

 // Função para calcular a resistencia interna de cada medição e preencher a tabela
function calcularResistencia() {
    var tabela = document.getElementById("tbDados");
   
    // Limpar tabela antes de preencher
    var tabelaHTML = `<tr>
                              <th>Pilha/Bateria</th>
                              <th>Tensão nominal (V)</th>
                              <th>Capacidade de corrente (mA.h)</th>
                              <th>Tensão sem carga(V)</th>
                              <th>Tensão com carga(V)</th>
                              <th>Resitência de carga(ohm)</th>
                              <th>Resistência interna(ohm)</th>
                    </tr>`;

// Iterar sobre os dados e calcular o r de cada um
    for (var i = 0; i < dados.length; i++) {
        var linha = dados[i];
        var E = linha.E;
        var V = linha.V;
        var R = linha.R;
        var r = R * (E / V - 1);
 
    // Adicionar nova linha à tabela com os valores e a soma
    var novaLinha = "<tr><td>" + linha.pilha + "</td><td>" + linha.tensao_nominal + "</td><td>" + linha.corrente + "</td><td>" + linha.E + "</td><td>" + linha.V + "</td><td>" + linha.R + "</td><td>" + r.toFixed(4) + "</td></tr>";
 
    tabelaHTML += novaLinha;
  }
  tabela.innerHTML = tabelaHTML;
}
 
calcularResistencia();